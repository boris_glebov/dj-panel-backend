# -*- coding: utf-8 -*-
__author__ = 'Boris'

from tornado import web
from tornadio2 import SocketConnection, TornadioRouter, SocketServer, event


class MellowaveConnetion(SocketConnection):

    def on_open(self, info):
        print "Client connected"

    def on_message(self, msg):
        print msg
        self.send(msg)

    def on_close(self):
        print "Client disconnected"

    @event('ping')
    def ping(self, name):
        print 'Got hello from %s' % name

        self.emit('pong', name)


router = TornadioRouter(MellowaveConnetion)

app = web.Application(router.urls, socket_io_port=8001)


if __name__ == '__main__':
    SocketServer(app)




