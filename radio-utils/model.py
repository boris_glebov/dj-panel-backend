# -*- coding: utf-8 -*-
__author__ = 'Boris'

from mongokit import Document, Connection, ObjectId
import settings

db = Connection(u"{0}:{1}".format(settings.MONGO_SERVER, settings.MONGO_PORT), replicaset='rs0')


class Entity(Document):
    __database__ = settings.MONGO_DATABASE_NAME

    use_dot_notation = True
    use_schemaless = True

    @property
    def id(self):
        return self._id


@db.register
class TrackEntity(Entity):
    __collection__ = "Tracks"

    structure_info = {
        'title': unicode,
        'artist': unicode,
        'vkid': unicode,
        'vk_url': unicode,
        'duration': int,
        'mbid': unicode,
        'file_name': unicode,
        'hash': unicode
    }

    required_fields = ['title', 'artist']

    #
    # Методы рассширения
    #

    @staticmethod
    def CreateTrackInfo(track):
        track_info = db.TrackEntity()

        for key in track:
            if key == "track_info":
                continue

            track_info[key] = track[key]

        if not "track_info" in track or track["track_info"] is None:
            track_info["_id"] = ObjectId()
        else:
            track_info["_id"] = track["track_info"]

        track_info["file_name"] = "{0}.mp3".format(track_info["_id"])

        return track_info

    @staticmethod
    def get_track_info(track):
        if not "track_info" in track or track["track_info"] is None:
            return None

        return db.TrackEntity.find_one({"_id": track["track_info"]})


@db.register
class RadioStationEntity(Entity):
    __collection__ = "RadioStation"

    structure_info = {
        'name': basestring,
        'group': basestring,
        'notes': basestring,
        'url': basestring,
        'playlists': [{
            'name': basestring,
            'path': basestring,
            'tracks': [{
                'vkid': basestring,
                'title': basestring,
                'artist': basestring,
                'vk_url': basestring,
                'duration': int,
                'track_info': TrackEntity
            }]
        }]
    }

    required_fields = ['name', 'group']

@db.register
class AudioFilesEntity(Entity):
    __collection__ = "AudioFiles"

    structure_info ={
        'Id': ObjectId,
        "Data": unicode
    }

    @staticmethod
    def GetByTrackId(trackId):
        return db.AudioFilesEntity.get({"_id": trackId})