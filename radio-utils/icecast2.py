# -*- coding: utf-8 -*-
__author__ = 'Boris'

import memcache
import settings

from urllib2 import urlopen, Request
from base64 import encodestring
from xml.dom import minidom
from model import db
from logger import *


class IcecastStat:
    def __init__(self, username, password, mount, host):
        self.username = username
        self.password = password
        self.url = settings.ICECAST_ADMIN_URL.format(host, mount)
        self.mount = mount

    def _get_xml_document(self):
        headers = {
            'Accept': 'application/xml',
            'Authorization': 'Basic %s' % (
                encodestring('%s:%s' % (self.username, self.password))[:-1]
            )
        }

        req = Request(self.url, headers=headers)
        res = urlopen(req)

        return res.read()

    def _get_stats(self):
        """
        Получаем информацию о точке
        """

        doc = minidom.parseString(self._get_xml_document())
        icestats = doc.getElementsByTagName('icestats')[0]

        source = icestats.getElementsByTagName('source')[0]
        data = {
            'server_name': source.getElementsByTagName('server_name')[0].firstChild.data,
            'server_type': source.getElementsByTagName('server_type')[0].firstChild.data,
            'listenurl': source.getElementsByTagName('listenurl')[0].firstChild.data,
            'title': source.getElementsByTagName('title')[0].firstChild.data
        }

        return data

    def get_current_song(self):
        """
        Возвращает название проигрываемого трэка
        """
        try:
            stats = self._get_stats()

            return stats["title"]
        except:
            logger.exception("Station '{0}' not found.".format(self.mount))
            return None


def decode_song(icecast_song):
    """
    В icecast название трэка имеет формат: <station_id>:<track_id>
    Данная функция получается настоящее значение из базы.

    Для оптимизации станции кэшируются
    """

    if icecast_song is None:
        return None

    separate_song_name = icecast_song.split('-')

    if len(separate_song_name) < 2:
        return icecast_song

    station_id = separate_song_name[0].strip()
    track_id = separate_song_name[1].strip()

    station = db.RadioStationEntity.find_one({'station_id': station_id})

    if station is None:
        logger.error("Station '{0}' not found".format(station))
        return icecast_song

    tracks = [track for track in station['tracks'] if track['hash'] == track_id]

    if len(tracks) == 0:
        return icecast_song
    else:
        return u"{0} - {1}|{2}".format(tracks[0]['artist'], tracks[0]['title'], track_id)


def main():
    memcache_client = memcache.Client(['{0}:{1}'.format(settings.MEMCACHED_SERVER, settings.MEMCACHED_PORT)])

    for station in db.RadioStationEntity.find():
        icecast_stat = IcecastStat(settings.ICECAST_ADMIN_USERNAME,
                                   settings.ICECAST_ADMIN_PASSWORD,
                                   '{0}.mp3'.format(station['station_id']), station['url'])

        current_song = decode_song(icecast_stat.get_current_song())

        if current_song is None:
            continue

        key = '{0}_tracks_cache'.format(station['station_id'])
        station_cache = memcache_client.get(key)

        if station_cache is None:
            station_cache = [current_song]
        elif station_cache[-1] != current_song:
            if len(station_cache) == settings.MAX_TRACKS_MEMORY:
                last_tracks = station_cache[1:]
                last_tracks.append(current_song)

                station_cache = last_tracks
            else:
                station_cache.append(current_song)

        print "STATION '{0}' HISTORY {1} tracks.".format(station['station_id'], len(station_cache))
        print station_cache
        print '###'

        memcache_client.set(key, station_cache)


if __name__ == '__main__':
    import time

    while True:
        try:
            main()
        except Exception as ex:
            print ex
            logger.exception("Exception: %s", ex)

        print "### Script end\n\n"
        time.sleep(1)














