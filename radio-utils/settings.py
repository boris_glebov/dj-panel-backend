# -*- coding: utf-8 -*-
__author__ = 'Boris'

from sys import platform as _platform

#
# ОБЩИЕ НАСТРОЙКИ ПРИЛОЖЕНИЯ
#

# Если приложение запущено в режиме разработки, выставить в True
IS_DEBUG = True

if _platform == "win32":
    WORKER_LOG_PATH = "c:\\logs\\worker.log"
else:
    WORKER_LOG_PATH = "/var/log/worker.log"

#
# НАСТРОЙКИ БАЗЫ ДАННЫХ (MongoDB)
#

if IS_DEBUG:
    # Название базы данных
    MONGO_DATABASE_NAME = "radio_db"
    # IP или DNS адрес сервера СУБД
    MONGO_SERVER = "rs0.mellowave.ru"
    # Порт сервера СУБД
    MONGO_PORT = 27017
else:
    # Название базы данных
    MONGO_DATABASE_NAME = "radio_db"
    # IP или DNS адрес сервера СУБД
    MONGO_SERVER = "rs0.mellowave.ru"
    # Порт сервера СУБД
    MONGO_PORT = 27017


#
# НАСТРОЙКИ МОДУЛЯ ПО РАБОТЕ С ICECAST 2
#

# кол-во запоминаемых трэков
MAX_TRACKS_MEMORY = 10

#
# MEMCACHED
#

if IS_DEBUG:
    MEMCACHED_SERVER = '192.168.1.100'
    MEMCACHED_PORT = '11211'
else:
    MEMCACHED_SERVER = '127.0.0.1'
    MEMCACHED_PORT = '11211'

#
# НАСТРОЙКИ ICECAST 2 (РАДИО СЕРВЕР)
#

ICECAST_ADMIN_URL = "{0}admin/stats?mount=/{1}"
ICECAST_ADMIN_USERNAME = "admin"
ICECAST_ADMIN_PASSWORD = "warava29A"
